var SDEFMODDIR;
var SDEFMOD;
var USESSION;
var UCOOKIE;
var KPORTID;
var KPORTVERID;
var DETAILVIEW = 10;
var SPREADSHEETVIEW = 20;
var KTLGLSTRING;
var glStringOptionsArray = new Array();
var API_URL_PREFIX;
var GLSELECTFIELDS;
var totalRows;

function loadGLStringSelects(timeCardID, glSet, intView, columnClassName, optionTemplate) {
    $.ajax({
        url: API_URL_PREFIX + "FORMID=A4APIGLSTRINGSET&KTIMECARDID=" + timeCardID + "&KGLSET=" + glSet,
        dataType: "text",
        type: "GET",
        success: function (data) {
            var result = parseJSON(data);
            if (!result) {
                alert("Unable to retrieve GL Strings.");
                return;
            }
            else {
                var glStrings = allButLast(result.glStrings);
                glStringOptionsArray = glStrings;
                addAutoCompleteComboBoxToTimeCard(intView, columnClassName, glStringOptionsArray, optionTemplate);
            }
        }
    });
}

function addAutoCompleteComboBoxToTimeCard(intView, columnClassName, glStringOptionsArray, optionTemplate) {

    var $dataRows = $("#mBody").find("tr");
    var totalRows = 0;

    $dataRows.each(function (index) {

        if (columnClassName == 'glStringCell') {
            var $glStringSelectorCell = $(this).find("td.glStringCell");

            if ($glStringSelectorCell != "undefined" && $glStringSelectorCell != null && $(this)[0].id != "heading") {
                //get the row ID
                var thisrowID = $(this)[0].id;

                //create the select element
                var selectID = "combo" + thisrowID;
                var $selectList = $('<select id="' + selectID + '">');

                //add empty row at start of list...
                $("<option />", { value: 0, text: "" }).appendTo($selectList);

                //add the options to the select element
                for (var i = 0; i < glStringOptionsArray.length; i++) {
                    //now we have to get the GL String setting for what the menu options should be, for example
                    //GLMASTERSTRING_ACCT_UNIT||'-'||GLMASTERSTRING_ACCOUNT||' '||GLMASTERSTRING_ACCT_UNIT_DESC||' - '||GLMASTERSTRING_DESC

                    //replace the GLMASTERSTRING text in the option string with the array and index and then the GLMASTERSTRING
                    var newTemplate = optionTemplate.replace(/GLMASTERSTRING/gi, "glStringOptionsArray[" + i + "].GLMASTERSTRING");

                    //if we're dealing with oracle, replace the || text in the array with +
                    newTemplate = newTemplate.replace(/\|\|/gi, "+");

                    //evaluate the string to get the option text for this select option
                    var optionText = eval(newTemplate);

                    //add the option to the select element with the value being the master string ID
                    //$selectList.append($("<option>").attr('value', glStringOptionsArray[i].GLMASTERSTRING_ID).text(optionText));
                    $("<option />", { value: glStringOptionsArray[i].GLMASTERSTRING_ID, text: optionText }).appendTo($selectList);

                }

                //now append the select list to the TD element in the table
                $glStringSelectorCell.append($selectList[0]);
                totalRows++;
            }
        }
    });

    convertSelectsToComboboxes(totalRows);

}

$(function (index) {
    $.widget("custom.combobox", {
        _create: function () {
            this.wrapper = $("<select>")
              .addClass("custom-combobox")
              .insertAfter(this.element);

            this.element.hide();
            this._createAutocomplete();
        },

        _createAutocomplete: function () {
            var selected = this.element.children(":selected"),
              value = selected.val() ? selected.text() : "";

            this.input = $("<input>")
             .appendTo(this.wrapper)
             .val(value)
             .attr("title", "")
             .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
             .autocomplete({
                 delay: 0,
                 minLength: 0,
                 source: $.proxy(this, "_source")
             })
             .tooltip({
                 classes: {
                     "ui-tooltip": "ui-state-highlight"
                 }
             });

            this._on(this.input, {
                autocompleteselect: function (event, ui) {
                    ui.item.option.selected = true;
                    this._trigger("select", event, {
                        item: ui.item.option
                    });
                },

                autocompletechange: "_removeIfInvalid"
            });
        },

        _source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response(this.element.children("option").map(function () {
                var text = $(this).text();
                if (this.value && (!request.term || matcher.test(text)))
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }));
        },

        _removeIfInvalid: function (event, ui) {
            // Selected an item, nothing to do
            if (ui.item) {
                return;
            }

            // Search for a match (case-insensitive)
            var value = this.input.val(),
              valueLowerCase = value.toLowerCase(),
              valid = false;

            this.element.children("option").each(function () {
                if ($(this).text().toLowerCase() === valueLowerCase) {
                    this.selected = valid = true;
                    return false;
                }
            });

            // Found a match, nothing to do
            if (valid) {
                return;
            }

            // Remove invalid value
            this.input
              .val("")
              .attr("title", value + " didn't match any item")
              .tooltip("open");

            this.element.val("");

            this._delay(function () {
                this.input.tooltip("close").attr("title", "");
            }, 2500);
            this.input.autocomplete("instance").term = "";
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });
});

function convertSelectsToComboboxes(totalRows) {
    for (var i = 0; i < totalRows; i++) {
        $("#comborow" + i).combobox();
    }
}